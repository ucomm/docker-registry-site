#!/bin/bash

rsync --dry-run -azvO -e ssh --chmod=ugo=rwX --no-perms \
  --exclude-from "excludes.txt" \
  --delete \
	./ jenkins@registry.ucdev.net:/var/www/net.ucdev.registry/public_html