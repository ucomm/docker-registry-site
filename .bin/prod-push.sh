#!/bin/bash

# require manual override of --dry-run on tagged pushes to prod
DRY_RUN=--dry-run

# while the jenkins param is boolean, it will evaluate as a string in this script
if [ "$RSYNC_DRY_RUN" = "false" ]
  then DRY_RUN=""
fi

rsync $DRY_RUN -azvO -e ssh --chmod=ugo=rwX --no-perms \
  --exclude-from "excludes.txt" \
  --delete \
	./ jenkins@registry.ucdev.net:/var/www/net.ucdev.registry/public_html