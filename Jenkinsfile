pipeline {
  agent any
  environment {
    DEV_BRANCH = "develop"
    PROD_BRANCH = "master"
  }
  parameters {
    booleanParam(
      name: 'RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag'
    )
  }
  stages {
    // checkout from git and skip if the "ci skip" message is present in the commit
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        // relies on the global notification library
        // https://bitbucket.org/ucomm/jenkins-send-notifications/src/main/
        sendNotifications 'STARTED'
      }
    }
    stage("Write credentials file") {
      steps {
        sh "${WORKSPACE}/.bin/auth"
      }
    }
    stage("Dev Push") {
      when {
        branch "${DEV_BRANCH}"
      }
      steps {
        sh "${WORKSPACE}/.bin/dev-push.sh"
      }
    }
    stage("Prod Push") {
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        sh "${WORKSPACE}/.bin/prod-push.sh"
      }
    }
    stage("Prod Start Docker") {
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        sh "${WORKSPACE}/.bin/prod-start.sh"
      }
    }
  }
  post {
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    aborted {
      sendNotifications 'ABORTED'
    }
  }
}