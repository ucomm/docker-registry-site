# Private docker registry and site.

## Registry
The registry is built using docker's `registry:2` image and exposes that image's API. To start using this registry, follow these steps.

### Setup and initial login
- `git clone` into a new directory
- `chmod +x ./.bin/auth` - make the ./.bin/auth file executable
- `./.bin/auth` - run the file to create basic authentication for the express server and registry
- `docker-compose up` to create the registry and start the server
- `docker login localhost:5000` - login to the server with admin/admin

### Adding an image to the registry
- `docker pull hello-world`
- `docker tag hello-world localhost:5000/hello/world` Note that `hello-world` and `hello/world` are different. Our naming convention will be to group images by using slashes (e.g. `uconn/cool-image`). On the front end, express will look for a readme at `/public/readmes/uconn/cool-image.md`
- Check that the new tag exists locally `docker images`
- `docker login localhost:5000` use the credentials in the `.env.js` file.
- `docker tag hello-world localhost:5000/hello/world` - create a reference that will be pushed to the registry
- `docker push localhost:5000/hello/world` - docker knows that `localhost:5000` is the location of the registry.

## Site
The frontend is served by express from the `/public` directory.

### Server
`server.js` proxies requests from the client to the registry container. the client can't directly address the registry without a server in between. this is because the registry is not available to the client at `localhost:5000` even though you can test the API with postman at that port. instead, because they're both on the docker network, requests need to go to `http://registry:5000`.

#### API
There are two read-only endpoints

- `/api/images` provides a list of all the images in the registry catalog. this is used to populate the select box
- `/api/images/:group/:name` fetches a single image's data (e.g. tags) and the `readme.md` for that image. The `:group` param refers to the directory the readme is stored in (e.g. /uconn)
